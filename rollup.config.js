import babel from "@rollup/plugin-babel"
import commonjs from "@rollup/plugin-commonjs"
import json from "@rollup/plugin-json"
import resolve from "@rollup/plugin-node-resolve"
import replace from "@rollup/plugin-replace"
import typescript from "@rollup/plugin-typescript"
import { spawn } from "child_process"
import colors from "kleur"
import { performance } from "perf_hooks"
import svelte from "rollup-plugin-svelte"
import { terser } from "rollup-plugin-terser"
import config from "sapper/config/rollup.js"

import pkg from "./package.json"

const { createPreprocessors } = require("./svelte.config.js")

const mode = process.env.NODE_ENV
const dev = mode === "development"
const sourcemap = dev ? "inline" : false
const legacy = !!process.env.SAPPER_LEGACY_BUILD

const preprocess = createPreprocessors({ sourceMap: !!sourcemap })

// Changes in these files will trigger a rebuild of the global CSS.
const globalCSSWatchFiles = [
  "postcss.config.js",
  "tailwind.config.js",
  "src/styles/index.pcss",
]

const onwarn = (warning, onwarn) => {
  switch (warning.code) {
    case "CIRCULAR_DEPENDENCY":
      if (/[/\\](@sapper)[/\\]/.test(warning.message)) {
        return
      }
      break
    case "MISSING_EXPORT":
      if (/'preload'/.test(warning.message)) {
        return
      }
      break
    case "PLUGIN_WARNING":
      if (warning.plugin === "css") {
        // This warning currently occurs when building service worker.
        return
      }
      break
    case "THIS_IS_UNDEFINED":
      return
  }
  console.log("onwarn", warning)
  return onwarn(warning)
}

export default {
  client: {
    input: config.client.input().replace(/\.js$/, ".ts"),
    output: { ...config.client.output(), sourcemap },
    plugins: [
      replace({
        "(process as any).browser": true,
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
      svelte({
        compilerOptions: {
          dev,
          hydratable: true,
          immutable: true,
        },
        emitCss: true,
        preprocess,
      }),
      resolve({
        browser: true,
        dedupe: ["svelte"],
      }),
      commonjs({ sourceMap: !!sourcemap }),
      typescript({ sourceMap: !!sourcemap }),

      legacy &&
        babel({
          babelHelpers: "runtime",
          exclude: ["node_modules/@babel/**"],
          extensions: [".js", ".mjs", ".html", ".svelte"],
          presets: [
            [
              "@babel/preset-env",
              {
                targets: "> 0.25%, not dead",
              },
            ],
          ],
          plugins: [
            "@babel/plugin-syntax-dynamic-import",
            [
              "@babel/plugin-transform-runtime",
              {
                useESModules: true,
              },
            ],
          ],
        }),

      !dev &&
        terser({
          module: true,
        }),

      (() => {
        let builder
        let rebuildNeeded = false

        const buildGlobalCSS = () => {
          if (builder) {
            rebuildNeeded = true
            return
          }
          rebuildNeeded = false
          const start = performance.now()

          try {
            builder = spawn("node", [
              "--experimental-modules",
              "--unhandled-rejections=strict",
              "build-global-css.mjs",
              sourcemap,
            ])
            builder.stdout.pipe(process.stdout)
            builder.stderr.pipe(process.stderr)

            builder.on("close", (code) => {
              if (code === 0) {
                const elapsed = parseInt(performance.now() - start, 10)
                console.log(
                  `${colors
                    .bold()
                    .green(
                      "✔ global css",
                    )} (src/styles/index.pcss → static/global.css${
                    sourcemap === true ? " + static/global.css.map" : ""
                  }) ${colors.gray(`(${elapsed}ms)`)}`,
                )
              } else if (code !== null) {
                if (dev) {
                  console.error(`global css builder exited with code ${code}`)
                  console.log(colors.bold().red("✗ global css"))
                } else {
                  throw new Error(`global css builder exited with code ${code}`)
                }
              }

              builder = undefined

              if (rebuildNeeded) {
                console.log(
                  `\n${colors
                    .bold()
                    .italic()
                    .cyan("something")} changed. rebuilding...`,
                )
                buildGlobalCSS()
              }
            })
          } catch (err) {
            console.log(colors.bold().red("✗ global css"))
            console.error(err)
          }
        }

        return {
          name: "build-global-css",
          buildStart() {
            buildGlobalCSS()
            globalCSSWatchFiles.forEach((file) => this.addWatchFile(file))
          },
          generateBundle: buildGlobalCSS,
        }
      })(),
    ],

    onwarn,
    preserveEntrySignatures: false,
  },

  server: {
    input: { server: config.server.input().server.replace(/\.js$/, ".ts") },
    output: { ...config.server.output(), sourcemap },
    plugins: [
      replace({
        "(process as any).browser": false,
        "process.browser": false,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
      svelte({
        compilerOptions: {
          dev,
          generate: "ssr",
          hydratable: true,
          immutable: true,
        },
        emitCss: false,
        preprocess,
      }),
      resolve({
        dedupe: ["svelte"],
      }),
      commonjs({
        sourceMap: !!sourcemap,
      }),
      typescript({ sourceMap: !!sourcemap }),
      json(),
    ],
    external:
      Object.keys(pkg.dependencies).concat(require("module").builtinModules) ||
      Object.keys(process.binding("natives")), // eslint-disable-line global-require,

    onwarn,
    preserveEntrySignatures: "strict",
  },

  serviceworker: {
    input: config.serviceworker.input().replace(/\.js$/, ".ts"),
    output: { ...config.serviceworker.output(), sourcemap },
    plugins: [
      resolve(),
      replace({
        "(process as any).browser": true,
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
      commonjs({ sourceMap: !!sourcemap }),
      typescript({ sourceMap: !!sourcemap }),
      !dev && terser(),
    ],

    preserveEntrySignatures: false,
    onwarn,
  },
}
