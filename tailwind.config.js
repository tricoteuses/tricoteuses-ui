const { tailwindExtractor } = require("tailwindcss/lib/lib/purgeUnusedStyles")

module.exports = {
  darkMode: false, // or 'media' or 'class'
  plugins: [],
  purge: {
    content: ["./src/**/*.html", "./src/**/*.svelte"],
    options: {
      defaultExtractor: (content) => [
        // This is an internal Tailwind function that we're not supposed to be allowed to use
        // So if this stops working, please open an issue at
        // https://github.com/babichjacob/sapper-postcss-template/issues
        // rather than bothering Tailwind Labs about it
        ...tailwindExtractor(content),
        // Match Svelte class: directives (https://github.com/tailwindlabs/tailwindcss/discussions/1731)
        ...[...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(
          ([_match, group, ..._rest]) => group,
        ),
      ],
      keyframes: true,
      safelist: [
        // /svelte-/,
        // For a17t:
        /^~/,
        /^!/,
      ],
    },
  },
  theme: {
    extend: {
      colors: {
        // Color taken from http://www.assemblee-nationale.fr/
        assemblee: "#023e6a",
        // Color taken from https://www.conseil-constitutionnel.fr/
        "conseil-constitutionnel": "#b29762",
        // Color taken from http://www.conseil-etat.fr/
        "conseil-etat": "#b1b3b4",
        // Color taken from https://www.gouvernement.fr/
        gouvernement: "#202328",
        // Color taken from https://www.elysee.fr/
        presidence: "#000",
        // Color taken from http://www.senat.fr
        senat: "#c81f48",
      },
    },
  },
  variants: {
    extend: {},
  },
}
