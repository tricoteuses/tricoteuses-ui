import compression from "compression"
import polka from "polka"
import sirv from "sirv"

import * as sapper from "@sapper/server"
import config from "./config"

const {
  amenda,
  assembleeApi,
  senatApi,
  hatvpApi,
  baseUrl,
  legislature,
  title,
} = config
const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const app = polka()
if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}
app
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      session: ((_req: Request, _res: Response) => {
        return {
          amenda,
          assembleeApi,
          senatApi,
          hatvpApi,
          baseUrl,
          legislature,
          title,
        }
      }) as () => unknown, // Quick & dirty hack to remove.
    }) as () => unknown, // Quick & dirty hack to remove.
  )
  .listen(PORT, (err: any) => {
    if (err) console.log("error", err)
  })
