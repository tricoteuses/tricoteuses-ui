import {
  Audit,
  auditFunction,
  auditRequire,
  auditTrimString,
  auditHttpUrl,
  cleanAudit,
  auditOptions,
  auditSwitch,
  auditTest,
} from "@auditors/core"
import { Legislature } from "@tricoteuses/assemblee"

export function auditConfig(audit: Audit, data: any): [any, any] {
  if (data == null) {
    return [data, null]
  }
  if (typeof data !== "object") {
    return audit.unexpectedType(data, "object")
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["amenda", "assembleeApi", "senatApi", "hatvpApi"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditWebServer,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "baseUrl",
    true,
    errors,
    remainingKeys,
    auditSwitch(
      auditHttpUrl,
      auditTest((value) => value === "."),
    ),
    auditRequire,
  )
  audit.attribute(
    data,
    "legislature",
    true,
    errors,
    remainingKeys,
    auditOptions([Legislature.Quatorze, Legislature.Quinze]),
    auditRequire,
  )
  audit.attribute(
    data,
    "title",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditWebServer(audit: Audit, data: any): [any, any] {
  if (data == null) {
    return [data, null]
  }
  if (typeof data !== "object") {
    return audit.unexpectedType(data, "object")
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditFunction((url: string) => url.replace(/\/+$/, "/")),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: any): [any, any] {
  return auditConfig(cleanAudit, data)
}
