require("dotenv").config()
import { validateConfig } from "./auditors/config"
const config = {
  amenda: {
    url: "http://localhost:1791",
  },
  assembleeApi: {
    url: "http://localhost:1789",
  },
  baseUrl: ".",
  legislature: "15",
  senatApi: {
    url: "http://localhost:1792",
  },
  hatvpApi: {
    //  url: "http://localhost:8002",
    url: "https://hatvp.api.tricoteuses.fr",
  },
  title: "Tricoteuses (dev)",
}
const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
export default validConfig
