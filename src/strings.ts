export {
  capitalizeFirstLetter,
  uncapitalizeFirstLetter,
} from "@tricoteuses/assemblee"

export function buildSearchUrl(
  url: string,
  { limit = 10, offset = 0, term = "" },
) {
  let query = []
  if (term) {
    query.push(
      `q=${encodeURIComponent(term).replace("(", " ").replace(")", " ")}`,
    )
  }
  if (offset !== 0) {
    query.push(`offset=${offset}`)
  }
  if (limit !== 10) {
    query.push(`limit=${limit}`)
  }
  let queryString = query.join("&")
  if (queryString) {
    queryString = (url.includes("?") ? "&" : "?") + query
  }
  return url + queryString
}
