import {
  Acteur,
  CodeTypeOrgane,
  Legislature,
  TypeMandat,
} from "@tricoteuses/assemblee"

export function getCommissionPermanente(
  acteur: Acteur,
  legislature: Legislature,
  now: Date,
) {
  const { mandats } = acteur
  if (mandats === undefined) {
    return null
  }
  for (const mandat of mandats) {
    if (
      mandat.xsiType === TypeMandat.MandatSimpleType &&
      mandat.legislature === legislature &&
      mandat.typeOrgane === CodeTypeOrgane.Comper &&
      mandat.dateDebut <= now &&
      (!mandat.dateFin || mandat.dateFin >= now)
    ) {
      return mandat.organes?.[0] ?? null
    }
  }
  return null
}

export function getGroupePolitique(
  acteur: Acteur,
  legislature: Legislature,
  now: Date,
) {
  const { mandats } = acteur
  if (mandats === undefined) {
    return null
  }
  for (const mandat of mandats) {
    if (
      mandat.xsiType === TypeMandat.MandatSimpleType &&
      mandat.legislature === legislature &&
      mandat.typeOrgane === CodeTypeOrgane.Gp &&
      mandat.dateDebut <= now &&
      (!mandat.dateFin || mandat.dateFin >= now)
    ) {
      return mandat.organes?.[0] ?? null
    }
  }
  return null
}
